To run the Naive Bayes & SVM models, run the following command:
For development scores: 
```
python3 nb_svm_reviews.py test
```
For test scores:
```
nb_svm_reviews.py dev
```

Requirements:  
- sklearn

To run the bi-LSTM model:
```
python3 bilstm.py
```
The first time running the code, it may take a while because it downloads the IMDb dataset and the Glove 6B embeddings.

Requirements:  
- nltk  
- pytorch  
- torchtext (pip install torchtext)

We have added code to ensure as much reproducibility as we could, but as Pytorch already mentions on their website (https://pytorch.org/docs/stable/notes/randomness.html):
"Completely reproducible results are not guaranteed across PyTorch releases, individual commits or different platforms. Furthermore, results need not be reproducible between CPU and GPU executions, even when using identical seeds."

All bi-LSTM experiments are run on the Peregrine cluster
