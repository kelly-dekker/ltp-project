import os
import random

directory_pos = 'aclImdb/train/pos'
directory_neg = 'aclImdb/train/neg'

random.seed(2019)


# Add all individual review text files into one textfile

with open('all.txt', 'w') as fw:
    for filename in os.listdir(directory_pos):
        with open(directory_pos + '/' + filename) as f:
                for line in f:
                    line = line.strip()
                    fw.write("{}<EOR>pos\n".format(line))
    for filename in os.listdir(directory_neg):
        with open(directory_neg + '/' + filename) as f:
            for line in f:
                line = line.strip()
                fw.write("{}<EOR>neg\n".format(line))


with open('all.txt') as source:
    data = [(random.random(), line) for line in source]
data.sort()
with open('train-shuffled.txt', 'w') as target:
    for _, line in data:
        target.write(line)
