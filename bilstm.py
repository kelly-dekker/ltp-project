import random
import numpy as np
import torch
import torch.nn as nn
from torchtext import data
from torchtext import datasets
import torch.optim as optim
import os
import json
import nltk
from nltk.corpus import stopwords
from model import RNN
from pytorchtools import EarlyStopping

seed = 1234

# Code for reproducibility from https://pytorch.org/docs/stable/notes/randomness.html
torch.manual_seed(seed)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
random.seed(seed)

nltk.download('stopwords')


def evaluate(model, iterator, criterion):
    epoch_loss = 0
    epoch_acc = 0

    model.eval()

    with torch.no_grad():
        for batch in iterator:
            text, text_lengths = batch.text

            predictions = model(text, text_lengths).squeeze(1)

            loss = criterion(predictions, batch.label)

            acc = binary_accuracy(predictions, batch.label)

            epoch_loss += loss.item()
            epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator)


def binary_accuracy(preds, y):
    """
    Returns accuracy per batch, i.e. if you get 8/10 right, this returns 0.8, NOT 8
    (Function from: https://github.com/bentrevett/pytorch-sentiment-analysis/blob/master/2%20-%20Upgraded%20Sentiment%20Analysis.ipynb)
    """
    #round predictions to the closest integer
    rounded_preds = torch.round(torch.sigmoid(preds))
    correct = (rounded_preds == y).float() #convert into float for division
    acc = correct.sum() / len(correct)
    return acc


def load_data():
    """
    Load the Imdb dataset. This function checks if dataset is already downloaded via torchtext datasets, see:
    https://torchtext.readthedocs.io/en/latest/datasets.html
    returns: X (preprocessed reviews as torchtext.data obj)
             y (labels as torch.float)
             train_data (train data as torchtext.dataset)
             test_data (test data as torchtext.dataset
    """
    if os.path.isfile('./.data/train.json') and os.path.isfile('./.data/test.json'):
        X = data.Field(lower=True, include_lengths=True, stop_words=stopwords.words('english'))
        y = data.LabelField(dtype=torch.float)

        fields = {'text': ('text', X), 'label': ('label', y)}

        train_data, test_data = data.TabularDataset.splits(
            path='.data',
            train='train.json',
            test='test.json',
            format='json',
            fields=fields
        )
        print("Data loaded")
        return X, y, train_data, test_data
    else:
        X = data.Field(lower=True, include_lengths=True, stop_words=stopwords.words('english'))
        y = data.Field(sequential=False, dtype=torch.float)

        print("Load Imdb data...")
        # make splits into train and test for data from Imdb Movie review dataset.
        train_data, test_data = datasets.IMDB.splits(X, y)

        train_examples = [vars(t) for t in train_data]
        test_examples = [vars(t) for t in test_data]

        with open('.data/train.json', 'w+') as f:
            for example in train_examples:
                json.dump(example, f)
                f.write('\n')

        with open('.data/test.json', 'w+') as f:
            for example in test_examples:
                json.dump(example, f)
                f.write('\n')

        print("Data loaded")
        return X, y, train_data, test_data


# Code i.a. from: https://torchtext.readthedocs.io/en/latest/datasets.html
# set up fields
X, y, train_data, test_data = load_data()

max_vocab_size = 25000
print("Build vocabulary with pretrained embeddings...")
X.build_vocab(train_data, max_size=max_vocab_size, vectors="glove.6B.100d", unk_init=torch.Tensor.normal_)
y.build_vocab(train_data)

print("Split data into train and validation...")
random_state = random.getstate()
train, valid = train_data.split(split_ratio=0.8, random_state=random_state)

vocab_size = len(X.vocab)  # = max_vocab size + 2 (padding and OOV token)

print("Training size: ", len(train))
print("Validation size: ", len(valid))
print("Test size: ", len(test_data))
print("Vocabulary size: ", vocab_size)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

batch_size = 64

# make iterator for splits
train_iter, valid_iter, test_iter = data.BucketIterator.splits(
    (train, valid, test_data), batch_size=batch_size, device=device, sort_key=lambda x:len(x.text), sort_within_batch=True)


input_size = vocab_size
embed_dim = 100
hidden_dim = 300
output_dim = 1
n_layers = 3
bidirectional = True
dropout = 0.5
pad_idx = X.vocab.stoi[X.pad_token]

print("Build model..")
model = RNN(input_size,
            embed_dim,
            hidden_dim,
            output_dim,
            n_layers,
            bidirectional,
            dropout,
            pad_idx)

print("Model: {}".format(model))
pretrained_embeddings = X.vocab.vectors

# print(pretrained_embeddings.shape)
model.embedding.weight.data.copy_(pretrained_embeddings)

unk_idx = X.vocab.stoi[X.unk_token]

model.embedding.weight.data[unk_idx] = torch.zeros(embed_dim)
model.embedding.weight.data[pad_idx] = torch.zeros(embed_dim)

lr = 0.001
optimizer = optim.Adam(model.parameters(), lr=lr)

# The BCEWithLogitsLoss function already includes sigmoid function,
# so this layer can be left out of the forward pass
criterion = nn.BCEWithLogitsLoss()

model = model.to(device)
criterion = criterion.to(device)

num_epochs = 5

best_valid_loss = float('inf')

# Code for stopping from: https://github.com/Bjarten/early-stopping-pytorch/blob/master/MNIST_Early_Stopping_example.ipynb
# early stopping commented out right now
# early_stopping = EarlyStopping(patience=10, verbose=True)

# Code for training, validation and testing based and inspired by following tutorials:
# https://www.dremio.com/tutorials/sentiment-analysis-with-pytorch-and-dremio/
# https://github.com/bentrevett/pytorch-sentiment-analysis
# https://github.com/clairett/pytorch-sentiment-classification/blob/master/train_batch.py
for epoch in range(num_epochs):
    train_loss = 0
    train_acc = 0
    batch_total = 0

    model.train()

    for batch in train_iter:

        optimizer.zero_grad()
        text, text_lengths = batch.text
        predictions = model(text, text_lengths).squeeze(1)

        loss = criterion(predictions, batch.label)

        acc = binary_accuracy(predictions, batch.label)

        loss.backward()

        optimizer.step()

        train_loss += loss.item()
        train_acc += acc.item()

    train_loss = train_loss / len(train_iter)
    train_acc = train_acc / len(train_iter)

    valid_loss, valid_acc = evaluate(model, valid_iter, criterion)

    if valid_loss < best_valid_loss:
        best_valid_loss = valid_loss
        torch.save(model.state_dict(), 'model.pt')

    # early_stopping needs the validation loss to check if it has decreased,
    # and if it has, it will make a checkpoint of the current model
    # early_stopping(valid_loss, model)

    # if early_stopping.early_stop:
    #     print("Early stopping")
    #     break

    print("End epoch {}.".format(epoch + 1))
    print("Train Loss: {:.3f} | Train Acc: {:.2f}".format(train_loss, train_acc * 100))
    print("Val. Loss: {:.3f} | Val. Acc: {:.2f}".format(valid_loss, valid_acc * 100))

model.load_state_dict(torch.load('model.pt'))
test_loss, test_acc = evaluate(model, test_iter, criterion)

print("Test Loss: {:.3f} | Test Acc: {:.2f}".format(test_loss, test_acc * 100))

# Output hyperparamters to terminal
print("Hyperparameters: \n"
      "Learning rate = {}\n"
      "Batch size = {}\n".format(lr, batch_size))
