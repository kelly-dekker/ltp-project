# This program splits the labels from the reviews
# Call the program by: split_reviews_from_labels.py *input_file.txt* *output_review_file.txt* *output_label_file.txt* no_stopwords (if you want to remove stopwords)

import sys
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize

def main(argv):
	
	stop_words = set(stopwords.words('english'))
	Xs = []
	Y = []
	out = open(argv[2], 'w')
	out_labels = open(argv[3], 'w')
	with open(argv[1]) as reviews:
		for line in reviews:
			line = line.split("<EOR>")
			review = line[0]
			try:
				if argv[4] == 'no_stopwords':
					review = word_tokenize(line[0])
					filt_review = [w for w in review if not w in stop_words] # https://www.geeksforgeeks.org/removing-stop-words-nltk-python/
					review = " ".join(filt_review)
			except:
				pass
			out.write(review)
			out.write("\n")
			out_labels.write(line[1])
	out.close()
	out_labels.close()
	
if __name__ == "__main__":
	main(sys.argv)
