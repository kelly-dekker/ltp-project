# !/usr/bin/env python3

# This program classifies reviews that are either positive or negative with Naive Bays and SVM
# To use entire training and data set: python3 nb_svm_reviews.py 
# To use development set: python3 nb_svm_reviews.py dev

import os
import re
import sys
import nltk
import pickle
import sklearn

from sklearn import svm
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score, classification_report
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

def main(argv):
	train_reviews, train_labels = get_data("train_reviews.txt", "train_labels.txt")
	test_reviews, test_labels = get_data("test_reviews.txt", "test_labels.txt")
	classify_with_scikit(train_reviews, train_labels, test_reviews, test_labels, argv[1])
	

def get_data(review_file, label_file):
	""" Imports the reviews and labels from txt files"""
	reviews = []
	labels = []
	
	#import reviews
	with open(review_file) as reviews_txt:
		for line in reviews_txt:
			reviews.append(line)
			
	#import labels
	with open(label_file) as labels_txt:
		for line in labels_txt:
			labels.append(line)
			
	return reviews, labels
	
		
def classify_with_scikit(train_reviews, train_labels, test_reviews, test_labels, data_set):	# Based on code provided by dr. L. Bosveld-de Smet and M. Nissim
	
	""" Prints accuracy and precision-recall-f1score table"""
	
	Xtrain = train_reviews
	Ytrain = train_labels
	Xtest = test_reviews
	Ytest = test_labels
	if data_set == "dev": # reduces data to create development set of training set
		X_dev = Xtrain[:int(0.2*len(Xtrain))]
		Y_dev = Ytrain[:int(0.2*len(Ytrain))]
		split_point = int(0.8*len(X_dev))
		Xtrain = X_dev[:split_point]
		Ytrain = Y_dev[:split_point]
		Xtest = X_dev[split_point:]
		Ytest = Y_dev[split_point:]
		
	vecNB = CountVectorizer()
	vecSVM = TfidfVectorizer()

	classifiers = [Pipeline( [('vec', vecNB),
						('cls', MultinomialNB())] ),
				   Pipeline( [('vec', vecSVM),
						('cls', svm.LinearSVC())] )]
	cur = 0
	for classifier in classifiers:
		classifier.fit(Xtrain, Ytrain)
		Yguess = classifier.predict(Xtest)
		if cur == 0: 
			print("Classification: Naive Bayes")
			print("Vectorizer: Count")
		else:
			print("Classification: SVM")
			print("Vectorizer: td-idf")
		print("Accuracy:",accuracy_score(Ytest, Yguess),'\n')
		print(classification_report(Ytest,Yguess))	
		print("\n\n\n")
		cur += 1

if __name__ == "__main__":
	main(sys.argv)

